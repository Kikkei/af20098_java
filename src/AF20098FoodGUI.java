import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AF20098FoodGUI {
    private JButton kakeButton;
    private JButton zaruButton;
    private JButton kamaageButton;
    private JButton curryButton;
    private JButton kitsuneButton;
    private JButton kamatamaButton;
    private JPanel root;
    private JLabel orderdfoods;
    private JTextPane textPane2;
    private JButton checkOutButton;
    private JLabel totalLabel;
    private JTabbedPane udon;
    private JButton Karaage;
    private JButton Ikaten;
    private JButton ChikuwaIsobeage;
    private JButton Ebiten;
    private JButton Toriten;
    private JButton Kakiage;
    private JLabel menue;


    int totalPrice=0;



    public AF20098FoodGUI() {

        kakeButton.setIcon(new ImageIcon(this.getClass().getResource("kakeudon.png")));
        kamaageButton.setIcon(new ImageIcon(this.getClass().getResource("kamaage.png")));
        kamatamaButton.setIcon(new ImageIcon(this.getClass().getResource("kamatama.png")));
        kitsuneButton.setIcon(new ImageIcon(this.getClass().getResource("kitsune.png")));
        zaruButton.setIcon(new ImageIcon(this.getClass().getResource("zaruudon.png")));
        curryButton.setIcon(new ImageIcon(this.getClass().getResource("curry.png")));

        totalLabel.setText("Total "+(String.format("%10d",totalPrice))+"yen");

        kakeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {

                order("Kake",270);
            }
        });
        zaruButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {

                order("Zaru",340);
            }
        });
        kamaageButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {

                order("Kamaage",350);
            }
        });
        kamatamaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {

                order("Kamatama",400);
            }
        });
        kitsuneButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {

                order("Kitsune",390);
            }
        });
        curryButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {

                order("Curry",590);
            }
        });


        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                int confirmation = JOptionPane.showConfirmDialog(
                        null,
                        "Would you like to check out?",
                        "Check out Confirmation",
                        JOptionPane.YES_NO_OPTION
                );
                if(confirmation==0){
                    JOptionPane.showMessageDialog(
                            null,
                            "Thank you. The total price is "+totalPrice+" yen."

                    );
                    totalPrice=0;
                    totalLabel.setText("Total "+(String.format("%10d",totalPrice))+"yen");
                    textPane2.setText("");
                }
            }
        });
        Toriten.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                {
                    order("Toriten",150);
                }

            }
        });
        Ebiten.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                {
                    order("Ebiten",160);
                }
            }
        });
        Kakiage.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                {
                    order("Kakiage",140);
                }
            }
        });
        ChikuwaIsobeage.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                {
                    order("ChikuwaIsobeage",110);
                }
            }
        });
        Ikaten.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                {
                    order("Ikaten",130);
                }
            }
        });
        Karaage.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                {
                    order("Karaage",120);
                }
            }
        });
    }

    void order(String food, int price){
        int confirmation = JOptionPane.showConfirmDialog(
                null,
                "Would you like to order "+food+" ?",
                "Order confirmation",
                JOptionPane.YES_NO_OPTION
        );

        if(confirmation == 0) {


            JOptionPane.showMessageDialog(
                    null,
                    "Thank you for ordering " + food + "! It will be served as soon as possible."
            );
            String currentText = textPane2.getText();
            textPane2.setText(currentText+food+" "+price+"yen\n");

            totalPrice+=price;
            totalLabel.setText("Total "+(String.format("%10d",totalPrice))+"yen");
        }


    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("AF20098FoodGUI");
        frame.setContentPane(new AF20098FoodGUI().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }


}


